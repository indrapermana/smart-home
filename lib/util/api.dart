class API {
  static const String _apiServer = "https://umkm.lovademica.com/api/";

  static const String login = _apiServer + "umkm_login";
  static const String checkEmail = _apiServer + "check_email";
  static const String dashboard = _apiServer + "dashboard";
  static const String kategori = _apiServer + "kategori";
  static const String team = _apiServer + "team";
  static const String umkmSubmit = _apiServer + "umkm_submit";
  static const String umkmRegister = _apiServer + "umkm_registrasi";
  static const String umkmUpdateUsaha = _apiServer + "umkm_update_usaha";
  static const String umkmValidate = _apiServer + "umkm_validate";
  static const String umkmGet = _apiServer + "umkm_get";
  static const String umkmProfile = _apiServer + "umkm_profile";
  static const String umkmUpdateProfile = _apiServer + "umkm_update_profile";
  static const String umkmUpdateFoto = _apiServer + "umkm_update_foto";
  static const String umkmUpdateSosmed = _apiServer + "umkm_update_sosmed";
  static const String survey = _apiServer + "survey";
  static const String surveyJawab = _apiServer + "survey_jawab";
  static const String materi = _apiServer + "materi";
  static const String materiVideoPlay = _apiServer + "materi_video_play";
  static const String task = _apiServer + "task";
  static const String taskVideo = _apiServer + "task_video";
  static const String taskStatus = _apiServer + "task_Status";
  static const String getProvinsi = _apiServer + "get_provinsi";
  static const String getKebupaten = _apiServer + "get_kabkota";
  static const String getKecamatan = _apiServer + "get_kecamatan";
  static const String getKelurahan = _apiServer + "get_kelurahan";
  static const String checkProfile = _apiServer + "check_profile";

}