import 'dart:ui';

class Palette {
  static Color error = Color(0xffe30018);

  static Color primary1 = Color(0xff4A86FD);

  static Color bgcolor = Color(0xffFFFFFF);
  static Color bgcolorContent = Color(0xff8A8A8A);
  static Color kuning = Color(0xffF9BD15);
  static Color hijau = Color(0xff60FC7C);

  // static List<Color> kitGradients = [primary1, primary2];

  // static var bordercolor = Color(0xffE2E4E6);
  static var bordercolor = Color(0xff3DB2FF);

  static var denomNoActive = Color(0xffD4D4D4);

  static var textColor = Color(0xff343640);

  static var labelgrey = Color(0xffB5B5B5);

  static var shadowbox = Color(0xffB7B7B7).withOpacity(0.16);
  static var shadowboxactive = Color(0xff4056C6).withOpacity(0.15);
}
