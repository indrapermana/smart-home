import 'package:flutter/widgets.dart';

class Level {
  static String check(String level){
    if (level!=null) { 
      if (level=="A") { return "Digital\nReady"; }
      else if (level=="B") { return "Digital Marketing\nReady"; }
      else if (level=="C") { return "Digital Production\nReady"; }
      else { return "Digital\nPreparation"; }
    }
    return "-";
  }

  static ImageProvider<Object> image(String level){
    if (level!=null) { 
      if (level=="A") { return AssetImage("assets/images/ilustrasi/mask_a.png"); }
      else if (level=="B") { return AssetImage("assets/images/ilustrasi/mask_b.png"); }
      else if (level=="C") { return AssetImage("assets/images/ilustrasi/mask_c.png"); }
      else { return AssetImage("assets/images/ilustrasi/mask_d.png"); }
    }
    return null;
  }
}