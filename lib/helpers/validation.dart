String validatorTelephone(value) {
  String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0 || value.isEmpty) {
    return "Tidak boleh kosong";
  } else if (!regExp.hasMatch(value)) {
    return "Masukan nomor handphone yang benar";
  }
  return null;
}

String validatorName(value) {
  String patttern = r'(^[a-zA-Z ]*$)';
  RegExp regExp = new RegExp(patttern);
  if (value.length == 0 || value.isEmpty) {
    return "Tidak boleh kosong";
  } else if (!regExp.hasMatch(value)) {
    return "Tidak boleh ada nomor / simbol";
  }
  return null;
}

String validatorEmail(value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0 || value.isEmpty) {
    return "Tidak boleh kosong";
  } else if (!regExp.hasMatch(value)) {
    return "Format email salah";
  } 
  return null;
}

String validatorAddress(value) {
  if (value.isEmpty) {
    return "Tidak boleh kosong";
  }
  if (value.length < 10) {
    return "Masukan alamat, min 10 karakter";
  }
  return null;
}