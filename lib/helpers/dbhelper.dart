import 'package:smart_home/modules/device/model/device.dart';
import 'package:smart_home/modules/room/model/room.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
//mendukug pemrograman asinkron
import 'dart:io';
//bekerja pada file dan directory
import 'package:path_provider/path_provider.dart';


//kelass Dbhelper
class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;  

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  Future<Database> initDb() async {
  
    //untuk menentukan nama database dan lokasi yg dibuat
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'smart_home.db';
    
    //create, read databases
    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);
    
    //mengembalikan nilai object sebagai hasil dari fungsinya
    return todoDatabase;
  }

  //buat tabel baru dengan nama room
  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE room (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        count_device TEXT
      )
    ''');
    
    /**
     * Jaringan
     * 1. Wi-fi
     * 2. Bluetooth
     * 3. IoT
     * 4. Bluetooth + Wi-fi
     * 5. Tidak Ada
     */
    await db.execute('''
      CREATE TABLE device (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_room INTEGER,
        name TEXT,
        nomor_serial TEXT,
        jaringan INTEGER,
        icon TEXT
      )
    ''');
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  //select room
  Future<List<Map<String, dynamic>>> selectRoom() async {
    Database db = await this.database;
    var mapList = await db.query('room', orderBy: 'name');
    return mapList;
  }

  //create room
  Future<int> insertRoom(Room object) async {
    Database db = await this.database;
    int count = await db.insert('room', object.toMap());
    return count;
  }

  //update room
  Future<int> updateRoom(Room object) async {
    Database db = await this.database;
    int count = await db.update('room', object.toMap(), where: 'id=?', whereArgs: [object.id]);
    return count;
  }

  //delete room
  Future<int> deleteRoom(int id) async {
    Database db = await this.database;
    int count = await db.delete('room', where: 'id=?', whereArgs: [id]);
    return count;
  }
  
  // list room
  Future<List<Room>> getRoomList() async {
    var roomMapList = await selectRoom();
    int count = roomMapList.length;
    List<Room> roomList = List<Room>();
    for (int i=0; i<count; i++) {
      roomList.add(Room.fromMap(roomMapList[i]));
    }
    return roomList;
  }

  //select Device
  Future<List<Map<String, dynamic>>> selectDevice() async {
    Database db = await this.database;
    var mapList = await db.query('device', orderBy: 'name');
    return mapList;
  }

  //select Device by room
  Future<List<Map<String, dynamic>>> selectDeviceByRoom(int idRoom) async {
    Database db = await this.database;
    var mapList = await db.query('device', where: 'id_room=?', whereArgs: [idRoom], orderBy: 'name');
    return mapList;
  }

  //create Device
  Future<int> insertDevice(Device object) async {
    Database db = await this.database;
    int count = await db.insert('device', object.toMap());
    return count;
  }

  //update Device
  Future<int> updateDevice(Device object) async {
    Database db = await this.database;
    int count = await db.update('device', object.toMap(), where: 'id=?', whereArgs: [object.id]);
    return count;
  }

  //delete Device
  Future<int> deleteDevice(int id) async {
    Database db = await this.database;
    int count = await db.delete('device', where: 'id=?', whereArgs: [id]);
    return count;
  }
  
  // list Device
  Future<List<Device>> getDeviceList() async {
    var mapList = await selectDevice();
    int count = mapList.length;
    List<Device> list = List<Device>();
    for (int i=0; i<count; i++) {
      list.add(Device.fromMap(mapList[i]));
    }
    return list;
  }

  // list Device
  Future<List<Device>> getDeviceListByRoom(int idRoom) async {
    var mapList = await selectDeviceByRoom(idRoom);
    int count = mapList.length;
    List<Device> list = List<Device>();
    for (int i=0; i<count; i++) {
      list.add(Device.fromMap(mapList[i]));
    }
    return list;
  }

}