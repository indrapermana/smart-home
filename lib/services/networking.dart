import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class NetworkHelper {
  NetworkHelper(this.url, this.jsonInput, this.token);

  final String url;
  final dynamic jsonInput;
  final String token;
  final _storage = FlutterSecureStorage();

  Future postForLogin() async {
    // Map<String, String> headersGet = header();

    // print('status koneksi');

    // bool statconn = await DataConnectionChecker().hasConnection;
    // print(statconn);
    // if (statconn) {
      try {
        print(url);
        // print(headersGet);
        print(jsonInput);

        http.Response response = await http.post(
          url,
          // headers: headersGet,
          body: jsonInput,
        );

        var decodedData = jsonDecode(response.body);

        print('ResponseAPI : ' + response.body);

        return decodedData;
      } catch (error) {
        print(error);
        var response = {
          "status" : 500,
          // "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
          "message" : error,
          "result" : null
        };
        return response;
      }
    // } else {
    //   print('Tidak Terhubung Dengan Server, Cek jaringan dan data');

    //   var response = {
    //     "status" : 500,
    //     "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
    //     "result" : null
    //   };
    //   return response;
    // }
  }

  Future postRequest() async {
    // Map<String, String> headersGet = await standardHeader();

    // bool statconn = await DataConnectionChecker().hasConnection;
    // print('status koneksi');
    // print(statconn);
    // if (statconn) {
      try {
        print(url);
        print("POST");
        // print(headersGet);
        print(jsonInput);

        http.Response response;
        // print(url);
        response = await http.post(
          url,
          // headers: headersGet,
          body: jsonInput,
        );

        var decodedData = jsonDecode(response.body);

        print(response.body);
        // if(decodedData["status"]==400){
        //   LoginManager.clearStorage();
        //   LoginProvider loadL = Provider.of<LoginProvider>(context, listen: false);
        //   loadL.validLogin = false;
        //   Navigator.pushReplacementNamed(context, "LoginScreen");
        // }

        return decodedData;
      } catch (e) {
        print(e);
        var response = {
          "status" : 500,
          "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
          "result" : null
        };
        return response;
      }
    // } else {
    //   print('Tidak Terhubung Dengan Server, Cek jaringan dan data');

    //   var response = {
    //     "status" : 500,
    //     "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
    //     "result" : null
    //   };
    //   return response;
    // }
  }

  Future getRequest() async {
    Map<String, String> headersGet = await standardHeader();

    // bool statconn = await DataConnectionChecker().hasConnection;
    // print('status koneksi');
    // print(statconn);

    // if (statconn) {
      try {
        print(url);
        print("GET");
        print(headersGet);

        http.Response responseget;
        print(url);
        responseget = await http.get(
          url,
          // headers: headersGet,
        );

        // if (responseget.statusCode == 200) {
          String data = responseget.body;
          var decodedData = jsonDecode(data);
          
          // print(responseget.statusCode);
          print(responseget.body);
          // if(decodedData["status"]==400){
          //   LoginManager.clearStorage();
          //   LoginProvider loadL = Provider.of<LoginProvider>(context, listen: false);
          //   loadL.validLogin = false;
          //   Navigator.pushReplacementNamed(context, "LoginScreen");
          // }
          return decodedData;
        // } else {
          // print(responseget.statusCode);
          // print(responseget.body);
        // }
      } catch (e) {
        print(e);
        var response = {
          "status" : 500,
          "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
          "result" : null
        };
        return response;
      }
    // } else {
    //   print('Tidak Terhubung Dengan Server, Cek jaringan dan data');

    //   var response = {
    //     "status" : 500,
    //     "message" : "Tidak ada jaringan internet ulangi beberapa saat lagi",
    //     "result" : null
    //   };
    //   return response;
    // }
  }

  Future<Map<String, String>> standardHeader() async {
    final data = await _storage.readAll();
    print(data);

    String tokens = data['token'];

    Map<String, String> headersGet = {
      'Content-Type': "application/json",
      'Accept': "application/json",
      'Authorization': tokens,
      'platform': Platform.isAndroid? 'android' : 'ios',
      'appversion': "1.5",
      'resolution': "2x",
      'API_KEY': "AKIAJMYZ5ZOUZGWJZYVQ",
      'timezone': '7'
    };

    return headersGet;
  }

  Map<String, String> header() {
    Map<String, String> headersGet = {
      "Accept": "application/json",
      "Content-Type": "application/json",
    };
    return headersGet;
  }
}
