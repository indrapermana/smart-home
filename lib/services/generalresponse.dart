class GeneralResponses {
  final status;
  final message;
  final uid;

  GeneralResponses({this.status, this.message, this.uid});

  GeneralResponses.fromJson(Map<String, dynamic> json)
      : status = json['status'].toString(),
        message = json['message'],
        uid = json['uid'];

  Map<String, dynamic> toJson() => {"status": status, "message": message, "uid": uid};
}
