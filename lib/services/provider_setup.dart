import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:smart_home/modules/device/provider/device_provider.dart';
import 'package:smart_home/modules/home/provider/home_provider.dart';
import 'package:smart_home/modules/room/provider/room_provider.dart';
import 'package:smart_home/modules/schedule/provider/schedule_provider.dart';

List<SingleChildStatelessWidget> providers = [
  ChangeNotifierProvider<HomeProvider>(create: (context) => HomeProvider()),
  ChangeNotifierProvider<ScheduleProvider>(create: (context) => ScheduleProvider()),
  ChangeNotifierProvider<RoomProvider>(create: (context) => RoomProvider()),
  ChangeNotifierProvider<DeviceProvider>(create: (context) => DeviceProvider()),
];