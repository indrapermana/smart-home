import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:smart_home/modules/device/view/device_add_screen.dart';
import 'package:smart_home/modules/home/view/home_detail_screen.dart';
import 'package:smart_home/modules/home/view/home_screen.dart';
import 'package:smart_home/modules/room/view/room_screen.dart';
import 'package:smart_home/modules/schedule/view/schedule_screen.dart';
import 'package:smart_home/modules/schedule/view/set_schedule_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case 'HomeScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => HomeScreen());
      break;
    case 'HomeDetailScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => HomeDetailScreen());
      break;
    case 'ScheduleScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => ScheduleScreen());
      break;
    case 'SetScheduleScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => SetScheduleScreen());
      break;
    case 'RoomScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => RoomScreen());
      break;
    case 'DeviceAddScreen':
      return MaterialPageRoute(
        settings: routeSettings(settings),
        builder: (context) => DeviceAddScreen());
      break;
    default:
      return MaterialPageRoute(
        settings: RouteSettings(name: 'HomeScreen'), 
        builder: (context) => HomeScreen()
      );
      break;
  }
}

RouteSettings routeSettings(RouteSettings settings) =>
    RouteSettings(name: settings.name);
