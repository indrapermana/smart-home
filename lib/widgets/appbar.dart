import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:flutter/material.dart';

class ReuseAppBar {
  static getAppBar(String title, BuildContext context, {Color bgColor, Color textColor, bool centerTitle, List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: centerTitle?? true,
      backgroundColor: bgColor?? Palette.primary1,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? InkWell(
        onTap: () => Navigator.pop(context),
        child: Icon(Icons.arrow_back, color: textColor??Colors.black,),
      ) : null,

      title: StandarText.label(title, 19.0, textColor),
      // flexibleSpace: Container(
      //   decoration: BoxDecoration(
      //     gradient: LinearGradient(
      //       begin: Alignment.centerRight,
      //       end: Alignment.centerLeft,
      //       colors: Palette.kitGradients,
      //       // colors: <Color>[Palette.primary1, Palette.primary2],
      //     ),
      //   ),
      // ),
      actions: actions,
    );
  }

  static getAppBarLogo(List<Widget> title, BuildContext context, {List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? InkWell(
        onTap: () => Navigator.pop(context),
        child: Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 2),
            borderRadius: BorderRadius.circular(100)
          ),
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
      ) : null,

      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 20.0),
            child: Image.asset("assets/images/logo/floPutih.png", width: 40,),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: title,
            )
          )
        ],
      ),
      // flexibleSpace: Container(
      //   decoration: BoxDecoration(
      //     gradient: LinearGradient(
      //       begin: Alignment.centerRight,
      //       end: Alignment.centerLeft,
      //       colors: Palette.kitGradients,
      //       // colors: <Color>[Palette.primary1, Palette.primary2],
      //     ),
      //   ),
      // ),
      actions: actions,
    );
  }

  static getAppBarColum(List<Widget> title, BuildContext context, {List<Widget> actions, bool leanding = true, void Function() onPressed}) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      leading: leanding ? InkWell(
        onTap: () => Navigator.pop(context),
        child: Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 2),
            borderRadius: BorderRadius.circular(100)
          ),
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
      ) : null,

      title: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: title,
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          // gradient: LinearGradient(
          //   begin: Alignment.centerRight,
          //   end: Alignment.centerLeft,
          //   colors: <Color>[Palette.primary2, Palette.primary1],
          // ),
        ),
      ),
      actions: actions,
    );
  }
}
