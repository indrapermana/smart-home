import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class ButtonColor extends StatelessWidget {
  final ScreenScaler scaler;
  final GestureTapCallback onPressed;
  final String text;
  final EdgeInsetsGeometry margin;
  final double width;
  final double height;
  final Color btnColor;
  final Color textColor;
  final double fontSize;

  ButtonColor({ this.scaler,  this.text,  this.onPressed, this.margin, this.width, this.height, this.btnColor, this.textColor, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin?? scaler.getMarginLTRB(0, 2, 0, 0),
      width: width?? scaler.getWidth(50),
      // height: height?? scaler.getWidth(7),
      padding: scaler.getPaddingAll(10),
      decoration: BoxDecoration(
        color: btnColor??Palette.primary1,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onPressed??(){},
          child: Center(
            child: StandarText.label(
              text??"",
              scaler.getTextSize(fontSize?? fontsz),
              textColor??Colors.black
            ),
          ),
        ),
      ),
    );
  }
}
