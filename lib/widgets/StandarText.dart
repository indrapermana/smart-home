import 'package:flutter/material.dart';
import 'package:smart_home/util/palette.dart';

class StandarText {
  static header1(judul, fontsize) {
    return Text(
      '$judul',
      style: TextStyle(
        fontSize: fontsize,
        // fontFamily: font,
        fontWeight: FontWeight.bold,
        color: Palette.textColor,
      ),
    );
  }

  static header2(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.left,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: Colors.white),
    );
  }

  static label(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: color??Palette.textColor),
    );
  }

  static labelCustom(judul, fontsize, {Color color, FontWeight fontWeight, TextAlign textAlign}) {
    return Text(
      '$judul',
      textAlign: textAlign??TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: fontWeight??FontWeight.normal,
          color: color??Palette.textColor),
    );
  }

  static labelRight(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.right,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: color??Palette.textColor),
    );
  }

  static labelCenter(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: fontsize,
        // fontFamily: font,
        fontWeight: FontWeight.bold,
        color: color??Palette.textColor
      ),
    );
  }

  static labelputih(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: Colors.white),
    );
  }

  static redlabel(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: Colors.red),
    );
  }

  static greenlabel(judul, fontsize) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
          fontSize: fontsize,
          // fontFamily: font,
          fontWeight: FontWeight.bold,
          color: Colors.green),
    );
  }

  static subtitle(judul, fontsize, [Color color]) {
    return Text(
      '$judul',
      textAlign: TextAlign.center,
      softWrap: true,
      style: TextStyle(
        fontSize: fontsize,
        // fontFamily: font,
//          fontWeight: FontWeight.bold,
        color: color?? Colors.grey,
      ),
    );
  }

  static labelgrey(String s, double textSize) {
    return Text(
      '$s',
      textAlign: TextAlign.left,
      softWrap: true,
      style: TextStyle(
        fontSize: textSize,
        // fontFamily: font,
        color: Palette.labelgrey,
        fontStyle: FontStyle.normal,
      ),
    );
  }
}
