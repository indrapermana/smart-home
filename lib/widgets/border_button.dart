import 'package:smart_home/util/constants.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';

class BorderButton extends StatelessWidget {

  final ScreenScaler scaler;
  final GestureTapCallback onPressed;
  final String text;
  final double width;
  final double height;
  final EdgeInsetsGeometry margin;
  final double fontSize;

  BorderButton({this.scaler,  this.text,  this.onPressed, this.width, this.height, this.margin, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin?? scaler.getMarginLTRB(0, 1, 0, 0),
      width: width?? scaler.getWidth(50),
      height: height?? scaler.getWidth(7),
      child: RaisedButton(
        onPressed: onPressed,
        child: StandarText.label(text, scaler.getTextSize(fontSize?? fontsz)),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Palette.bordercolor),
        ),
        color: Colors.white,
      ),
    );
  }
}