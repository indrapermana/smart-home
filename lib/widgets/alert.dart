import 'package:smart_home/util/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/button_color.dart';

void alertDialog({BuildContext context,  ScreenScaler scaler, String msg, Widget body, String title, Color color, Color textColor, double height, List<Widget> actions, GestureTapCallback onPressed}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        // title: title!=null? Text(title, style: TextStyle(color: color??Colors.red, fontSize: scaler.getTextSize(12)), textAlign: TextAlign.center,) : null,
        contentPadding: scaler.getPaddingLTRB(2, 1.1, 2, 1.1),
        content: body?? Container(
          height: height?? scaler.getHeight(25),
          width: scaler.getWidth(100),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              title!=null? StandarText.labelCustom(title, scaler.getTextSize(11), textAlign: TextAlign.center, color: color) : Container(),
              title!=null? SizedBox(height: 10,) : Container(),
              StandarText.labelCustom(
                msg??"", scaler.getTextSize(11),
                fontWeight: FontWeight.normal, color: textColor??Colors.black,
                textAlign: TextAlign.center
              ),
              Container(
                width: scaler.getWidth(100),
                margin: scaler.getMarginLTRB(0, 2, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: actions??[
                    ButtonColor(
                      height: scaler.getWidth(7),
                      scaler: scaler,
                      text: "Oke",
                      btnColor: Palette.primary1, 
                      textColor: Colors.black,
                      onPressed: onPressed?? () => Navigator.pop(context)
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );
    },
  );
}