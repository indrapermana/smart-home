import 'package:smart_home/router/router.dart' as router;
import 'package:smart_home/util/constants.dart';
import 'package:smart_home/services/provider_setup.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: titleApp,
        theme: ThemeData(
          primarySwatch: Colors.blue
        ),
        onGenerateRoute: router.generateRoute,
        initialRoute: 'SplashScreen',
      ),
    );
  }
}