import 'package:flutter/widgets.dart';
import 'package:smart_home/modules/room/model/room.dart';

class RoomProvider with ChangeNotifier {
  bool _isLoading = false;
  String _message = "";

  String _nameRoom = "";
  List<Room> _listRoom = []; 

  bool get isLoading => _isLoading;
  String get message => _message;

  String get nameRoom => _nameRoom;
  List<Room> get listRoom => _listRoom;

  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  set message(String val) {
    _message = val;
    notifyListeners();
  }

  set nameRoom(String val){
    _nameRoom = val;
    notifyListeners();
  }

  set listRoom(List<Room> val) {
    _listRoom = val;
    notifyListeners();
  }

  void saveRoom(){
    int count = listRoom.length + 1;
    listRoom.add(Room(
      count,
      nameRoom,
      "0"
    ));
    notifyListeners();
  }
}