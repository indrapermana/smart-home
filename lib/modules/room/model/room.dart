class Room {
  int _id;
  String _name;
  String _countDevice;

  // konstruktor versi 1
  Room(this._id, this._name, this._countDevice);

  // konstruktor versi 2: konversi dari Map ke Room
  Room.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._name = map['name'];
    this._countDevice = map['count_device'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  String get name => _name;
  String get countDevice => _countDevice;

  // setter  
  set name(String value) {
    _name = value;
  }

  set countDevice(String value) {
    _countDevice = value;
  }

  // konversi dari Room ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['name'] = name;
    map['count_device'] = countDevice;
    return map;
  }  

}