import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/device/provider/device_provider.dart';
import 'package:smart_home/modules/room/provider/room_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/alert.dart';

class RoomScreen extends StatefulWidget {
  const RoomScreen({ Key key }) : super(key: key);

  @override
  _RoomScreenState createState() => _RoomScreenState();
}

class _RoomScreenState extends State<RoomScreen> {

  void showModal(ScreenScaler scaler){
    alertDialog(
      context: context, 
      scaler: scaler,
      body: Consumer<RoomProvider>(
        builder: (context, load, _) => Container(
          height: 150,
          padding: scaler.getPaddingAll(10),
          child: Column(
            children: [
              StandarText.label("Buat Ruangan Baru", scaler.getTextSize(10)),
              Container(
                margin: scaler.getMarginLTRB(0, 1, 0, 2),
                child: TextFormField(
                  // controller: username,
                  onChanged: (val) => load.nameRoom = val,
                  validator: (value) {
                    int len = value.length;
                    if (len == 0) {
                      return "Nama Ruangan Can not be empty";
                    }
                    return null;
                  },
                  style: TextStyle(color: Colors.black54),
                  decoration: InputDecoration(
                    hintText: '',
                    labelText: 'Nama Ruangan',
                    labelStyle: TextStyle(
                      color: Colors.black54,
                      fontFamily: 'Barlow',
                      fontWeight: FontWeight.bold
                    ),
                    // prefixIcon: Icon(
                    //   Icons.person,
                    //   color: Colors.black54,
                    // ),
                    fillColor: Colors.black54,
                    errorStyle: TextStyle(
                      color: Colors.black54
                    ),
                    contentPadding: EdgeInsets.only(left: 10),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(
                        color: Palette.primary1,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(
                        color: Palette.bordercolor,
                        width: 2.0,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(
                        color: Palette.primary1,
                        width: 2.0,
                      ),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: BorderSide(
                        color: Palette.primary1,
                        width: 2.0,
                      ),
                    )
                  ),
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: StandarText.labelCenter("Batal", scaler.getTextSize(10), Palette.primary1),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      load.saveRoom();
                    },
                    child: StandarText.labelCenter("Simpan", scaler.getTextSize(10), Palette.primary1),
                  )
                ],
              )
            ],
          ),
        ),
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Pilih Ruangan", style: TextStyle(color: Colors.black),),
        // automaticallyImplyLeading: false,
        iconTheme: IconThemeData(color: Colors.black),
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.black,), 
            onPressed: () => showModal(scaler)
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Consumer<RoomProvider>(
        builder: (context, load, _) => SingleChildScrollView(
          padding: scaler.getPaddingLTRB(3, 1, 3, 0),
          child: Wrap(
            spacing: 15,
            runSpacing: 15,
            children: load.listRoom.map((data) => content(scaler, data.id, data.name, data.countDevice)).toList(),
            // children: [
            //   content(scaler, "Kamar Tidur", 0),
            //   content(scaler, "Kamar Tidur", 0),
            //   content(scaler, "Kamar Tidur", 0),
            // ],
          ),
        ),
      ),
    );
  }

  Widget content(ScreenScaler scaler, int id, String name, String device){
    return InkWell(
      onTap: (){
        DeviceProvider loadD = Provider.of(context, listen: false);
        loadD.idRoom = id;
        Navigator.pushNamed(context, "DeviceAddScreen");
      },
      child: Container( 
        width: scaler.getWidth(40),
        padding: scaler.getPaddingAll(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Palette.shadowbox,
              blurRadius: 5.0, // soften the shadow
              spreadRadius: 2.0, //extend the shadow
              offset: Offset(
                0.2, // Move to right 10  horizontally
                1.0, // Move to bottom 5 Vertically
              ),
            )
          ],
        ),
        child: Column(
          children: [
            StandarText.label(name, scaler.getTextSize(10)),
            SizedBox(height: 10,),
            StandarText.labelCustom("$device Device", scaler.getTextSize(9)),
          ],
        ),
      ),
    );
  }
}