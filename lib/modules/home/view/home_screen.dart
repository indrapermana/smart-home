import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/device/provider/device_provider.dart';
import 'package:smart_home/modules/home/provider/home_provider.dart';
import 'package:smart_home/modules/home/view/device.dart';
import 'package:smart_home/modules/room/provider/room_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/alert.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return FutureBuilder(
    //   future: FlutterBluetoothSerial.instance.requestEnable(),
    //   builder: (context, future) {
    //     if (future.connectionState == ConnectionState.waiting) {
    //       return Scaffold(
    //         body: Container(
    //           height: double.infinity,
    //           child: Center(
    //             child: Icon(
    //               Icons.bluetooth_disabled,
    //               size: 200.0,
    //               color: Colors.blue,
    //             ),
    //           ),
    //         ),
    //       );
    //     } else if (future.connectionState == ConnectionState.done) {
    //       // return MyHomePage(title: 'Flutter Demo Home Page');
    //       return HomeStandart();
    //     } else {
    //       return HomeStandart();
    //     }
    //   },
    //   // child: MyHomePage(title: 'Flutter Demo Home Page'),
    // );
    return HomeStandart();
  }
}

class HomeStandart extends StatefulWidget {
  const HomeStandart({ Key key }) : super(key: key);

  @override
  _HomeStandartState createState() => _HomeStandartState();
}

enum DeviceAvailability {
  no,
  maybe,
  yes,
}

class DeviceWithAvailability extends BluetoothDevice {
  BluetoothDevice device;
  DeviceAvailability availability;
  int rssi;

  DeviceWithAvailability(this.device, this.availability, [this.rssi]);
}

class _HomeStandartState extends State<HomeStandart> {

  List<DeviceWithAvailability> devices = List<DeviceWithAvailability>();

  // Availability
  StreamSubscription<BluetoothDiscoveryResult> _discoveryStreamSubscription;
  String selected;
  bool status = false;
  

  @override
  void initState() {
    super.initState();
    // bluetooth();
  }

  void bluetooth(){
    HomeProvider load = Provider.of(context, listen: false);
    load.getBluetooth(context);
  }

  void startDiscovery() {
    _discoveryStreamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
      setState(() {
        Iterator i = devices.iterator;
        while (i.moveNext()) {
          var _device = i.current;
          if (_device.device == r.device) {
            _device.availability = DeviceAvailability.yes;
            _device.rssi = r.rssi;
          }
        }
      });
    });

    _discoveryStreamSubscription.onDone(() {
      print("bluetooth active");
    });
  }

  @override
  void dispose() {
    // Avoid memory leak (`setState` after dispose) and cancel discovery
    _discoveryStreamSubscription?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      // backgroundColor: Palette.primary1,
      body: Consumer<HomeProvider>(
        builder: (context, load, _) => ModalProgressHUD(
          opacity: 0.5,
          inAsyncCall: load.isLoading,
          child: SingleChildScrollView(
            padding: scaler.getPaddingLTRB(3, 4, 3, 0),
            child: Column(
              children: [
                Container(
                  margin: scaler.getMarginLTRB(0, 0, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset("assets/images/logo/logo.png"),
                      Expanded(
                        child: StandarText.labelCenter("smarti", scaler.getTextSize(11)),
                        // child: Column(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     // StandarText.labelCustom("Smarti", scaler.getTextSize(9), color: Colors.white),
                        //     StandarText.label("smarti", scaler.getTextSize(11), Colors.white)
                        //   ],
                        // )
                      ),
                      IconButton(
                        icon: Icon(Icons.add, size: scaler.getTextSize(14),), 
                        // onPressed: () => Navigator.pushNamed(context, "DeviceAddScreen")
                        onPressed: () => Navigator.pushNamed(context, "RoomScreen")
                      )
                    ],
                  ),
                ),

                Consumer<RoomProvider>(
                  builder: (context, loadR, _) => Container(
                    margin: scaler.getMarginLTRB(0, 0, 0, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        IconButton(
                          icon: Icon(Icons.more_vert), 
                          onPressed: (){}
                        ),
                        Expanded(
                          child: DropdownButton<String>(
                            focusColor:Colors.white,
                            value: selected,
                            //elevation: 5,
                            style: TextStyle(color: Colors.white),
                            iconEnabledColor:Colors.black,
                            items: loadR.listRoom.map((data) => DropdownMenuItem<String>(
                              value: data.id.toString(),
                              child: Text(data.name),
                            )).toList(),
                            // items: <String>[
                            //   'TV',
                            //   'AC',
                            //   'Kulkas',
                            //   'Mesin Cuci',
                            //   'Lampu',
                            // ].map<DropdownMenuItem<String>>((String value) {
                            //   return DropdownMenuItem<String>(
                            //     value: value,
                            //     child: Text(value,style:TextStyle(color:Colors.black),),
                            //   );
                            // }).toList(),
                            hint:Text(
                              "All Device",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                            onChanged: (String value) {
                              setState(() {
                                selected = value;
                              });
                              DeviceProvider loadD = Provider.of(context, listen: false);
                              loadD.idRoom = int.parse(value);
                              loadD.getDeviceByRoom();
                            },
                          ),
                        ),
                        Switch(
                          activeColor: Palette.primary1,
                          value: load.active, 
                          onChanged: (val) {
                            print("status $status");
                            // if(status == true){
                              setState(() {
                                status = val;
                              });
                              // load.active = val;
                              // if(load.active){
                              //   print("nyala");
                              //   load.sendMessage("r");
                              // } else {
                              //   print("nyala");
                              //   load.sendMessage("s");
                              // }
                            // }
                          }
                        )
                      ],
                    ),
                  ),
                ),

                Consumer<DeviceProvider>(
                  builder: (context, loadD, _) => Container(
                    child: Column(
                      children: loadD.listSearchDevice.length>0? 
                      loadD.listSearchDevice.map((data) => content(scaler, data.name, load.isConnected)).toList()
                      : loadD.listDevice.map((data) => content(scaler, data.name, load.isConnected)).toList(),
                    ),
                  ),
                ),
                // content(scaler, "Lampu Utama", load.isConnected),
                // content(scaler, "Lampu Dapur", false),
                
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Palette.shadowbox,
              blurRadius: 5.0, // soften the shadow
              spreadRadius: 2.0, //extend the shadow
              offset: Offset(
                0.2, // Move to right 10  horizontally
                1.0, // Move to bottom 5 Vertically
              ),
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.home),
                  StandarText.label("Home", scaler.getTextSize(10))
                ],
              ),
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.settings),
                  StandarText.label("Setting", scaler.getTextSize(10))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget content(ScreenScaler scaler, String title, bool status){
    return Consumer<HomeProvider>(
      builder: (context, load, _) => InkWell(
        onTap: () {
          if(status){
            load.title = title;
            Navigator.pushNamed(context, "HomeDetailScreen");
          } else {
            load.alertModalBluetooth(context);
          }
        },
        child: Container( 
          margin: scaler.getMarginLTRB(0, 0, 0, 1),
          padding: scaler.getPaddingAll(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Palette.shadowbox,
                blurRadius: 5.0, // soften the shadow
                spreadRadius: 2.0, //extend the shadow
                offset: Offset(
                  0.2, // Move to right 10  horizontally
                  1.0, // Move to bottom 5 Vertically
                ),
              )
            ],
          ),
          child: Row(
            children: [
              Image.asset("assets/images/icon/lampu.png"),
              SizedBox(width: 10,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    StandarText.label(title, scaler.getTextSize(10)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [ 
                        Container(
                          width: 10,
                          height: 10, 
                          decoration: BoxDecoration(
                            color: status? Colors.green : Colors.red,
                            borderRadius: BorderRadius.circular(100)
                          ),
                        ), 
                        SizedBox(width: 5,),
                        StandarText.labelCustom(status? "Connected" : "Disconnect", scaler.getTextSize(8.5))
                      ],
                    )
                  ],
                ),
              ),
              Switch(
                activeColor: Palette.primary1,
                value: load.active, 
                onChanged: (val) {
                  print("status $status");
                  if(status == true){
                    load.active = val;
                    if(load.active){
                      print("nyala");
                      load.sendMessage("r");
                    } else {
                      print("nyala");
                      load.sendMessage("s");
                    }
                  }
                }
              )
            ],
          ),
        ),
      ),
    );
  }
}