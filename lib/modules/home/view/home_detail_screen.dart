import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/home/provider/home_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/appbar.dart';

class HomeDetailScreen extends StatefulWidget {
  const HomeDetailScreen({ Key key }) : super(key: key);

  @override
  _HomeDetailScreenState createState() => _HomeDetailScreenState();
}

class _HomeDetailScreenState extends State<HomeDetailScreen> {

  TimeOfDay _time;
  Timer _timer;
  int selisih = 0;

  @override
  void initState() {
    super.initState();
    // connectBluetooth();
    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
    //   HomeProvider load = Provider.of<HomeProvider>(context, listen: false);
    //   load.connectBluetooth();
    // });
  }

  void _selectTime() async {
    if(_timer!=null){
      setState(() {
        _timer.cancel();
      });
    }
    final TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: _time?? TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.dial
    );
    if (newTime != null) {
      startTimer(newTime.format(context));
      setState(() {
        _time = newTime;
      });
    }
  }

  void startTimer(String time) {
    // String timeNow = TimeOfDay.now().format(context);
    var tm = time.split(":");
    // var tmNow = timeNow.split(":");


    DateTime dateNow = DateTime.now();
    DateTime dateStop = DateTime(dateNow.year, dateNow.month, dateNow.day, int.parse(tm[0]), int.parse(tm[1]));
    selisih = dateStop.difference(dateNow).inSeconds;
    print("selisih : $selisih");
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (selisih == 0) {
          HomeProvider load = Provider.of(context, listen: false);
          if (load.active) {
            load.sendMessage("s");
            load.active = false;
          } else {
            load.sendMessage("r");
            load.active = true;
          }
          
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            selisih--;
          });
          print("selisih : $selisih");
        }
      },
    );
  }

  void connectBluetooth(){
    HomeProvider load = Provider.of(context, listen: false);
    print(load.bluetoothDevice.address);
    BluetoothConnection.toAddress(load.bluetoothDevice.address).then((_connection) {
      print('Connected to the device');
      load.connection = _connection;
      load.isConnecting = false;
      load.isDisconnecting = false;

      load.connection.input.listen(load.onDataReceived).onDone(() {
        // Example: Detect which side closed the connection
        // There should be `isDisconnecting` flag to show are we are (locally)
        // in middle of disconnecting process, should be set before calling
        // `dispose`, `finish` or `close`, which all causes to disconnect.
        // If we except the disconnection, `onDone` should be fired as result.
        // If we didn't except this (no flag set), it means closing by remote.
        if (load.isDisconnecting) {
          print('Disconnecting locally!');
        } else {
          print('Disconnected remotely!');
        }
        // if (this.mounted) {
        //   // setState(() {});
        //   print(this.mounted);
        // }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      appBar: ReuseAppBar.getAppBar(
        Provider.of<HomeProvider>(context).title, 
        context, 
        bgColor: Palette.primary1, 
        textColor: Colors.white,
      ),
      backgroundColor: Palette.primary1,
      body: Consumer<HomeProvider>(
        builder: (context, load, _) => Container(
          width: scaler.getWidth(100),
          height: scaler.getHeight(100),
          padding: scaler.getPaddingLTRB(3, 1, 3, 0),
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  load.active = !load.active;
                  if(load.active){
                    load.sendMessage("r");
                  } else {
                    load.sendMessage("s");
                  }      
                },
                child: Container(
                  width: scaler.getWidth(100),
                  height: _time!=null? scaler.getHeight(55) : scaler.getHeight(70),
                  child: Center(
                    child: Container(
                      width: scaler.getWidth(50),
                      height: scaler.getWidth(50),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                            color: Palette.shadowbox,
                            blurRadius: 5.0, // soften the shadow
                            spreadRadius: 2.0, //extend the shadow
                            offset: Offset(
                              0.2, // Move to right 10  horizontally
                              1.0, // Move to bottom 5 Vertically
                            ),
                          )
                      ],
                      ),
                      child: Center(
                        child: Image.asset(load.active? "assets/images/icon/lamp_active.png" : "assets/images/icon/lamp_not_active.png"),
                      ),
                    ),
                  ),
                ),
              ),

              _time!=null? Container(
                margin: scaler.getMarginLTRB(0, 0, 0, 5),
                child: StandarText.labelCenter("Timer : ${_time.format(context)}", scaler.getTextSize(12), Colors.white),
              ) : Container(),
              
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () => _selectTime(),
                      child: Container(
                        padding: scaler.getPaddingAll(10),
                        width: scaler.getWidth(30),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(50)
                        ),
                        child: StandarText.labelCenter("Timer", scaler.getTextSize(10)),
                      ),
                    ),
                    InkWell(
                      onTap: () => Navigator.pushNamed(context, "ScheduleScreen"),
                      child: Container(
                        padding: scaler.getPaddingAll(10),
                        width: scaler.getWidth( 30),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(50)
                        ),
                        child: StandarText.labelCenter("Schedule", scaler.getTextSize(10)),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}