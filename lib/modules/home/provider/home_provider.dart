
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:smart_home/modules/home/view/device.dart';
import 'package:smart_home/modules/home/view/home_screen.dart';
import 'package:smart_home/widgets/StandarText.dart';

class HomeProvider with ChangeNotifier {
  bool _isLoading = false;
  String _message = "";
  bool _active = false;
  String _title = "";

  List<DeviceWithAvailability> _devices = List<DeviceWithAvailability>();
  BluetoothDevice _bluetoothDevice;
  BluetoothConnection _connection;

  bool isConnecting = true;

  bool _isDisconnecting = false;
  String _messageBuffer = '';

  bool get isLoading => _isLoading;
  String get message => _message;
  bool get active => _active;
  String get title => _title;

  List<DeviceWithAvailability> get devices => _devices;
  BluetoothDevice get bluetoothDevice => _bluetoothDevice;
  BluetoothConnection get connection => _connection;
  bool get isConnected => _connection != null && _connection.isConnected;
  bool get isDisconnecting => _isDisconnecting;
  String get messageBuffer => _messageBuffer;

  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  set message(String val) {
    _message = val;
    notifyListeners();
  }

  set active(bool val) {
    _active = val;
    notifyListeners();
  }

  set title(String val) {
    _title = val;
    notifyListeners();
  }

  set devices(List<DeviceWithAvailability> val){
    _devices = val;
    notifyListeners();
  }

  set bluetoothDevice(BluetoothDevice val){
    _bluetoothDevice = val;
    notifyListeners();
  }

  set connection(BluetoothConnection val){
    _connection = val;
    notifyListeners();
  }

  set isDisconnecting(bool val){
    _isDisconnecting = val;
    notifyListeners();
  }

  set messageBuffer(String val){
    _messageBuffer = val;
    notifyListeners();
  }

  Future<void> dispose() async {
    if (isConnected) {
      isDisconnecting = true;
      connection.dispose();
      connection = null;
    }
  }

  void getBluetooth(BuildContext context){
    FlutterBluetoothSerial.instance.getBondedDevices().then((List<BluetoothDevice> bondedDevices) {
      print(bondedDevices);
      devices = bondedDevices.map((device) => DeviceWithAvailability(
          device,
          DeviceAvailability.yes,
        ),
      )
      .toList();
      if (!isConnected) {
        alertModalBluetooth(context);
      }
      notifyListeners();
    });
  }

  Future<void> connectBluetooth() async {
    print('Connecting to the device');
    print("Address : ${bluetoothDevice.address}");
    BluetoothConnection.toAddress(bluetoothDevice.address).then((_connection) {
      print('Connected to the device');
      isLoading = false;
      connection = _connection;
      isConnecting = false;
      isDisconnecting = false;

      connection.input.listen(onDataReceived).onDone(() {
        // Example: Detect which side closed the connection
        // There should be `isDisconnecting` flag to show are we are (locally)
        // in middle of disconnecting process, should be set before calling
        // `dispose`, `finish` or `close`, which all causes to disconnect.
        // If we except the disconnection, `onDone` should be fired as result.
        // If we didn't except this (no flag set), it means closing by remote.
        if (isDisconnecting) {
          print('Disconnecting locally!');
        } else {
          print('Disconnected remotely!');
        }
        // if (this.mounted) {
        //   // setState(() {});
        //   print(this.mounted);
        // }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });
  }

  void onDataReceived(Uint8List data) {
    // Allocate buffer for parsed data
    print(data);
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    // Apply backspace control character
    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      } else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        } else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }
  }

  void sendMessage(String text) async {
    text = text.trim();
    print(text);

    if (text.length > 0) {
      try {
        connection.output.add(utf8.encode(text + "\r\n"));
        await connection.output.allSent;
      } catch (e) {
        // Ignore error, but notify state
        // setState(() {});
      }
    }
  }

  void alertModalBluetooth(BuildContext context){
    showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
          content: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*0.5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                StandarText.labelCustom("Silahkan Connect Bluetooth anda", 12.0, textAlign: TextAlign.center),
                SizedBox(height: 10,),
                Column(
                  children: devices.map((data) => BluetoothDeviceListEntry(
                    device: data.device,
                    onTap: () async {
                      Navigator.pop(context);
                      isLoading = true;
                      bluetoothDevice = data.device;
                      await connectBluetooth();
                      // sendMessage("r");
                      // active = true;
                      notifyListeners();
                    },
                  )).toList(),
                )
              ],
            ),
          ),
        );
      }
    );
  }
}