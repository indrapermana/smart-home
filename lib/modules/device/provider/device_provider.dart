import 'package:flutter/widgets.dart';
import 'package:smart_home/modules/device/model/device.dart';

class DeviceProvider with ChangeNotifier {
  bool _isLoading = false;
  String _message = "";

  List<Device> _listDevice = [];
  List<Device> _listSearchDevice = [];
  int _idRoom;
  String _selectIcon = "";
  String _nomorSerial = "";
  String _name = "";
  int _jaringan;

  bool get isLoading => _isLoading;
  String get message => _message;

  List<Device> get listDevice => _listDevice;
  List<Device> get listSearchDevice => _listSearchDevice;
  int get idRoom => _idRoom;
  String get selectIcon => _selectIcon;
  String get nomorSerial => _nomorSerial;
  String get name => _name;
  int get jaringan => _jaringan;

  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  set message(String val) {
    _message = val;
    notifyListeners();
  }

  set listDevice(List<Device> val) {
    _listDevice = val;
    notifyListeners();
  }

  set listSearchDevice(List<Device> val){
    _listSearchDevice = val;
    notifyListeners();
  }

  set idRoom(int val){
    _idRoom = val;
    notifyListeners();
  }

  set selectIcon(String val){
    _selectIcon = val;
    notifyListeners();
  }

  set nomorSerial(String val){
    _nomorSerial = val;
    notifyListeners();
  }

  set name(String val){
    _name = val;
    notifyListeners();
  }

  set jaringan(int val){
    _jaringan = val;
    notifyListeners();
  }

  void saveDevice(){
    listDevice.add(Device(
      idRoom,
      name,
      nomorSerial,
      jaringan,
      selectIcon
    ));
    notifyListeners();
  }

  void getDeviceByRoom(){
    for (var item in listDevice) {
      if(item.id == idRoom){
        listSearchDevice.add(item);
      }
    }
    notifyListeners();
  }
}