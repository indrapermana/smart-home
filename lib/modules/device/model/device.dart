class Device {
  int _id;
  int _idRoom;
  String _name;
  String _nomorSerial;
  int _jaringan;
  String _icon;

  // konstruktor versi 1
  Device(this._idRoom, this._name, this._nomorSerial, this._jaringan, this._icon);

  // konstruktor versi 2: konversi dari Map ke Room
  Device.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._idRoom = map['id_room'];
    this._name = map['name'];
    this._nomorSerial = map['nomor_serial'];
    this._jaringan = map['jaringan'];
    this._icon = map['icon'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  int get idRoom => _idRoom;
  String get name => _name;
  String get nomorSerial => _nomorSerial;
  int get jaringan => _jaringan;
  String get icon => _icon;

  // setter  
  set idRoom(int value) {
    _idRoom = value;
  }

  set name(String value) {
    _name = value;
  }

  set nomorSerial(String value) {
    _nomorSerial = value;
  }

  set jaringan(int value) {
    _jaringan = value;
  }

  set icon(String value) {
    _icon = value;
  }

  // konversi dari Room ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['id_room'] = _idRoom;
    map['name'] = name;
    map['nomor_serial'] = nomorSerial;
    map['jaringan'] = jaringan;
    map['icon'] = icon;
    return map;
  }  

}