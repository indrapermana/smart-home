import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/device/provider/device_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/alert.dart';
import 'package:smart_home/widgets/button_color.dart';

class DeviceAddScreen extends StatefulWidget {
  const DeviceAddScreen({ Key key }) : super(key: key);

  @override
  _DeviceAddScreenState createState() => _DeviceAddScreenState();
}

class _DeviceAddScreenState extends State<DeviceAddScreen> {
  bool showDetail = false;
  String _character = "";
  String selected = "";

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Pilih Device", 
        style: TextStyle(color: Colors.black), ), 
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black,),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: scaler.getHeight(100),
              color: Palette.bgcolorContent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: scaler.getPadding(3, 5),
                    color: Colors.white,
                    child: StandarText.label("Elektronik Rumah", scaler.getTextSize(10)),
                  ),
                  Container(
                    padding: scaler.getPadding(3, 5),
                    child: StandarText.label("Lampu", scaler.getTextSize(10)),
                  ),
                  Container(
                    padding: scaler.getPadding(3, 5),
                    child: StandarText.label("Kelistrikan Rumah", scaler.getTextSize(10)),
                  ),
                  Container(
                    padding: scaler.getPadding(3, 5),
                    child: StandarText.label("CCTV", scaler.getTextSize(10)),
                  ),
                ],
              ),
            ),
            Expanded(
              child: showDetail? formDevice(scaler) : listDevice(scaler)
            )
          ],
        ),
      ),
    );
  }

  Widget listDevice(ScreenScaler scaler){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        InkWell(
          onTap: (){
            setState(() {
              showDetail = true;
            });
          },
          child: Container(
            margin: scaler.getMarginLTRB(0, 1, 0, 0),
            padding: scaler.getPaddingAll(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                StandarText.labelCenter("Ac Conditioner / AC", scaler.getTextSize(11)),
                Divider(color: Palette.bgcolorContent,),
                Container(
                  child: Wrap(
                    spacing: 10,
                    runSpacing: 10,
                    children: [
                      Image.asset("assets/images/icon/device/ac.png")
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          margin: scaler.getMarginLTRB(0, 1, 0, 0),
          padding: scaler.getPaddingAll(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              StandarText.labelCenter("Kulkas", scaler.getTextSize(11)),
              Divider(color: Palette.bgcolorContent,),
              Container(
                // padding: scaler.getPaddingAll(10),
                child: Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    Image.asset("assets/images/icon/device/kulkas.png")
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin: scaler.getMarginLTRB(0, 1, 0, 0),
          padding: scaler.getPaddingAll(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              StandarText.labelCenter("Mesin Cuci", scaler.getTextSize(11)),
              Divider(color: Palette.bgcolorContent,),
              Container(
                // padding: scaler.getPaddingAll(10),
                child: Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    Image.asset("assets/images/icon/device/mesin_cuci.png")
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          margin: scaler.getMarginLTRB(0, 1, 0, 0),
          padding: scaler.getPaddingAll(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              StandarText.labelCenter("TV", scaler.getTextSize(11)),
              Divider(color: Palette.bgcolorContent,),
              Container(
                // padding: scaler.getPaddingAll(10),
                child: Wrap(
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    Image.asset("assets/images/icon/device/tv.png")
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
  
  Widget formDevice(ScreenScaler scaler){
    return Consumer<DeviceProvider>(
      builder: (context, load, _) => Container(
        padding: scaler.getPaddingAll(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StandarText.labelCustom("Air Conditioner / AC", scaler.getTextSize(8)),
            SizedBox(height: 10,),
            StandarText.label("Pilih AC", scaler.getTextSize(9)),
            SizedBox(height: 10,),
            Container(
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                children: [
                  InkWell(
                    onTap: (){
                      load.selectIcon = "1";
                    },
                    child: Container(
                      padding: scaler.getPaddingAll(8),
                      decoration: BoxDecoration(
                        border: load.selectIcon=="1"? Border.all(color: Palette.bordercolor) : null,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Image.asset("assets/images/icon/device/ac_1.png"),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      load.selectIcon = "2";
                    },
                    child: Container(
                      padding: scaler.getPaddingAll(8),
                      decoration: BoxDecoration(
                        border: load.selectIcon=="2"? Border.all(color: Palette.bordercolor) : null,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Image.asset("assets/images/icon/device/ac_2.png"),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      load.selectIcon = "3";
                    },
                    child: Container(
                      padding: scaler.getPaddingAll(8),
                      decoration: BoxDecoration(
                        border: load.selectIcon=="3"? Border.all(color: Palette.bordercolor) : null,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Image.asset("assets/images/icon/device/ac_3.png"),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      load.selectIcon = "4";
                    },
                    child: Container(
                      padding: scaler.getPaddingAll(8),
                      decoration: BoxDecoration(
                        border: load.selectIcon=="4"? Border.all(color: Palette.bordercolor) : null,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: Image.asset("assets/images/icon/device/ac_4.png"),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      load.selectIcon = "5";
                    },
                    child: Container(
                      padding: scaler.getPaddingAll(8),
                      decoration: BoxDecoration(
                        border: load.selectIcon=="5"? Border.all(color: Palette.bordercolor) : null,
                        borderRadius: BorderRadius.circular(8)
                      ),
                      child: Image.asset("assets/images/icon/device/ac_5.png"),
                    ),
                  ),
                ],
              ),
            ),
            
            Container(
              margin: scaler.getMarginLTRB(0, 1, 0, 1),
              child: TextFormField(
                // controller: username,
                onChanged: (val) => load.nomorSerial = val,
                validator: (value) {
                  int len = value.length;
                  if (len == 0) {
                    return "Nomor Serial Can not be empty";
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black54),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: '',
                  labelText: 'Nomor Serial',
                  labelStyle: TextStyle(
                    color: Colors.black54,
                    fontFamily: 'Barlow',
                    fontWeight: FontWeight.bold
                  ),
                  // prefixIcon: Icon(
                  //   Icons.person,
                  //   color: Colors.black54,
                  // ),
                  fillColor: Colors.black54,
                  errorStyle: TextStyle(
                    color: Colors.black54
                  ),
                  contentPadding: EdgeInsets.only(left: 10),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.bordercolor,
                      width: 2.0,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                      width: 2.0,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                      width: 2.0,
                    ),
                  )
                ),
              ),
            ),
            Container(
              margin: scaler.getMarginLTRB(0, 1, 0, 1),
              child: TextFormField(
                // controller: username,
                onChanged: (val) => load.name = val,
                validator: (value) {
                  int len = value.length;
                  if (len == 0) {
                    return "Nama Can not be empty";
                  }
                  return null;
                },
                style: TextStyle(color: Colors.black54),
                decoration: InputDecoration(
                  hintText: '',
                  labelText: 'Nama',
                  labelStyle: TextStyle(
                    color: Colors.black54,
                    fontFamily: 'Barlow',
                    fontWeight: FontWeight.bold
                  ),
                  // prefixIcon: Icon(
                  //   Icons.person,
                  //   color: Colors.black54,
                  // ),
                  fillColor: Colors.black54,
                  errorStyle: TextStyle(
                    color: Colors.black54
                  ),
                  contentPadding: EdgeInsets.only(left: 10),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.bordercolor,
                      width: 2.0,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                      width: 2.0,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: Palette.primary1,
                      width: 2.0,
                    ),
                  )
                ),
              ),
            ),
            StandarText.label("Pilih Jaringan", scaler.getTextSize(9)),
            Container(
              child: Column(
                children: [
                  Container(
                    padding: scaler.getPadding(0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 1,
                          groupValue: load.jaringan,
                          onChanged: (value) {
                            load.jaringan = value;
                          },
                        ),
                        StandarText.labelCustom("Wi-Fi", scaler.getTextSize(10)),
                      ],
                    ),
                  ),
                  Container(
                    padding: scaler.getPadding(0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 2,
                          groupValue: load.jaringan,
                          onChanged: (value) {
                            load.jaringan = value;
                          },
                        ),
                        StandarText.labelCustom("Bluetooth", scaler.getTextSize(10)),
                      ],
                    ),
                  ),
                  Container(
                    padding: scaler.getPadding(0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 3,
                          groupValue: load.jaringan,
                          onChanged: (value) {
                            load.jaringan = value;
                          },
                        ),
                        StandarText.labelCustom("IoT", scaler.getTextSize(10)),
                      ],
                    ),
                  ),
                  Container(
                    padding: scaler.getPadding(0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 4,
                          groupValue: load.jaringan,
                          onChanged: (value) {
                            load.jaringan = value;
                          },
                        ),
                        StandarText.labelCustom("Bluetooth + Wi-Fi", scaler.getTextSize(10)),
                      ],
                    ),
                  ),
                  Container(
                    padding: scaler.getPadding(0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 5,
                          groupValue: load.jaringan,
                          onChanged: (value) {
                            load.jaringan = value;
                          },
                        ),
                        StandarText.labelCustom("Tidak Ada", scaler.getTextSize(10)),
                      ],
                    ),
                  ),
                  ButtonColor(
                    scaler: scaler,
                    text: "Tambah",
                    btnColor: Palette.primary1,
                    textColor: Colors.white,
                    width: scaler.getWidth(30),
                    onPressed: (){
                      load.saveDevice();
                      alertDialog(
                        context: context,
                        scaler: scaler,
                        msg: "Sukses",
                        onPressed: (){
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        }
                      );
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}