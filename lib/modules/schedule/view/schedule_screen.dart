import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/schedule/provider/schedule_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/appbar.dart';

class ScheduleScreen extends StatelessWidget {
  const ScheduleScreen({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      appBar: ReuseAppBar.getAppBar(
        "Schedule", 
        context, 
        bgColor: Palette.primary1, 
        textColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.white,), 
            onPressed: () => Navigator.pushNamed(context, "SetScheduleScreen")
          )
        ]
      ),
      backgroundColor: Palette.primary1,
      body: Consumer<ScheduleProvider>(
        builder: (context, load, _) => SingleChildScrollView(
          padding: scaler.getPaddingLTRB(3, 2, 3, 0),
          child: Column(
            children: [
              Container(
                padding: scaler.getPaddingAll(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          StandarText.label("13:00", scaler.getTextSize(10)),
                          StandarText.labelCustom("Everyday", scaler.getTextSize(8)),
                          SizedBox(height: 10,),
                          StandarText.labelCustom("Switch", scaler.getTextSize(8)),
                          StandarText.label(load.active? "ON" : "OFF", scaler.getTextSize(10)),
                        ],
                      ),
                    ),
                    Expanded(
                      child: StandarText.labelCustom("Lorem ipsum dolor sit amet", scaler.getTextSize(10))
                    ),
                    Switch(
                      value: load.active, 
                      onChanged: (val) => load.active = val
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}