import 'package:flutter/material.dart';
import 'package:flutter_screen_scaler/flutter_screen_scaler.dart';
import 'package:provider/provider.dart';
import 'package:smart_home/modules/schedule/provider/schedule_provider.dart';
import 'package:smart_home/util/palette.dart';
import 'package:smart_home/widgets/StandarText.dart';
import 'package:smart_home/widgets/appbar.dart';
import 'package:smart_home/widgets/button_color.dart';

class SetScheduleScreen extends StatefulWidget {
  const SetScheduleScreen({ Key key }) : super(key: key);

  @override
  _SetScheduleScreenState createState() => _SetScheduleScreenState();
}

class _SetScheduleScreenState extends State<SetScheduleScreen> {

  TimeOfDay time;

  void selectTime(BuildContext context) async {
    final TimeOfDay newTime = await showTimePicker(
      context: context,
      initialTime: time?? TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.input
    );
    if (newTime != null) {
      setState(() {
        time = newTime;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenScaler scaler = ScreenScaler()..init(context);
    return Scaffold(
      appBar: ReuseAppBar.getAppBar("Set Schedule", context, textColor: Colors.white,),
      backgroundColor: Palette.primary1,
      body: Consumer<ScheduleProvider>(
        builder: (context, load, _) => SingleChildScrollView(
          padding: scaler.getPaddingAll(10),
          child: Column(  
            children: [
              InkWell(
                onTap: () => selectTime(context),
                child: Container(
                  padding: scaler.getPaddingAll(10),
                  margin: scaler.getMarginLTRB(0, 0, 0, 1),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      StandarText.label("Setting Time", scaler.getTextSize(10)),
                      SizedBox(height: 5,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: StandarText.labelCustom(time!=null? time.format(context) : '-', scaler.getTextSize(10))
                          ),
                          StandarText.label("Setting", scaler.getTextSize(9), Palette.primary1),
                        ],
                      )
                    ],
                  ),
                ),
              ),

              Container(
                padding: scaler.getPaddingAll(10),
                margin: scaler.getMarginLTRB(0, 0, 0, 1),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StandarText.label("Repeat", scaler.getTextSize(10)),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              StandarText.labelCustom("Once", scaler.getTextSize(10), color: Palette.labelgrey),
                              Icon(Icons.keyboard_arrow_right, color: Palette.labelgrey,)
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StandarText.label("Note", scaler.getTextSize(10)),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              StandarText.labelCustom("None", scaler.getTextSize(10), color: Palette.labelgrey),
                              Icon(Icons.keyboard_arrow_right, color: Palette.labelgrey,)
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StandarText.label("Notivication", scaler.getTextSize(10)),
                        Switch(
                          value: load.notification, 
                          onChanged: (val) => load.notification = val
                        )
                      ],
                    )
                  ],
                ),
              ),

              Container(
                padding: scaler.getPaddingAll(10),
                margin: scaler.getMarginLTRB(0, 0, 0, 1),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    StandarText.label("Note", scaler.getTextSize(10)),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          StandarText.labelCustom("None", scaler.getTextSize(10), color: Palette.labelgrey),
                          Icon(Icons.keyboard_arrow_right, color: Palette.labelgrey,)
                        ],
                      ),
                    )
                  ],
                ),
              ),

              ButtonColor(
                scaler: scaler, 
                text: "Save", 
                btnColor: Colors.white,
                textColor: Palette.primary1,
                width: scaler.getWidth(100),
                margin: scaler.getMarginLTRB(0, 0, 0, 0),
                onPressed: () => Navigator.pop(context)
              )
            ],
          ),
        )
      )
    );
  }
}