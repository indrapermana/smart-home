
import 'package:flutter/widgets.dart';

class ScheduleProvider with ChangeNotifier {
  bool _isLoading = false;
  String _message = "";
  bool _active = false;
  bool _notification = false;

  bool get isLoading => _isLoading;
  String get message => _message;
  bool get active => _active;
  bool get notification => _notification;

  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  set message(String val) {
    _message = val;
    notifyListeners();
  }

  set active(bool val) {
    _active = val;
    notifyListeners();
  }

  set notification(bool val) {
    _notification = val;
    notifyListeners();
  }
}